import { Component } from '@angular/core';

//@ts-ignore
const parentProperties = window.xprops;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public section = parentProperties?.parentLocation;

  public onButtonClick () {
    parentProperties?.onASuperSpecificEvent("SOME_DATA_GENERATED_INSIDE_ISOLATED_CHILD_COMPONENT");
  }
}
