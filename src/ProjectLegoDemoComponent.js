var ProjectLegoDemoComponent = zoid.create({
  tag: 'project-lego-demo-component',
  url: 'http://localhost:4200',
  dimensions: {
      width: '100%',
      height: '100%'
  },
  props: {
    authenticationKey: {
        type: 'string',
        required: true
    },
    parentLocation: {
        type: 'string',
        required: true
    },
    onASuperSpecificEvent: {
        type: 'function',
        required: false
    }
  }
});
